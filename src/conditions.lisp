(in-package :crbk)

(define-condition size-exceeds-sequence-length-error (error)
  ((size-expression :initarg :size-expression
                    :reader size-expression)
   (size :initarg :size
         :reader size)
   (sequence-expression :initarg :sequence-expression
                        :reader sequence-expression)
   (sequence-length :initarg :sequence-length
                    :reader sequence-length))
  (:report (lambda (condition stream)
             (format stream
                     "~a exceeds length of sequence ~a~%"
                     (if (eq (size-expression condition)
                             (size condition))
                         (format nil "Value ~a"
                                 (size condition))
                         (format nil "Expression ~a"
                                 (size-expression condition)))
                     (sequence-expression condition)))))

(defun cap-size-to-sequence-length (c)
  (let ((*print-escape* nil))
    (print-object c *error-output*))
  (format *error-output*
          "...substituting ~a for ~a~%"
          (sequence-length c)
          (size c))
  (invoke-restart 'cap-size-to-sequence-length))

(define-condition open-file-error (file-error)
  ((direction :initarg :direction
              :reader direction))
  (:report (lambda (condition stream)
             (format stream
                     "Could not open file ~a for ~a. Either
 1) the file does not exist, or
 2) we are not allowed to access the file, or
 3) the pathname points to a broken symbolic link.~%"
                     (file-error-pathname condition)
                     (case (direction condition)
                       (:input "reading")
                       (:output "writing")
                       (:io "bidirectional i/o")
                       (:probe "probing")
                       (otherwise "unknown i/o operation"))))))

(defun report-and-quit (c)
  (let ((*print-escape* nil))
    (print-object c *error-output*)
    (format *error-output*
            "~&An error has occured and the program will be terminated~%"))
  (ext:quit 1))
